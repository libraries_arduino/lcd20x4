#include "LCD20x4.h"
#include "stdio.h"
#include "stdarg.h"

#define CMD_Clear_disp  0x01
#define CMD_Return_Home 0x02
#define CMD_Entry_Mode  0x04
#define CMD_Disp_Ctrl   0x08
#define CMD_Cursor_SH   0x10
#define CMD_Func_Set    0x20
#define CMD_Set_CGRAM	0x40
#define CMD_Set_DDRAM	0x80

const uint8_t char_addr[4][20] = {	{0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13},
									{0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4D,0x4E,0x4F,0x50,0x51,0x52,0x53},
									{0x14,0x15,0x16,0x17,0x18,0x19,0x1A,0x1B,0x1C,0x1D,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27},
									{0x54,0x55,0x56,0x57,0x58,0x59,0x5A,0x5B,0x5C,0x5D,0x5E,0x5F,0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67} };

void portWrite(uint32_t LSB_pin, uint32_t value) {
	for (uint8_t i = 0; i < 8; i++) digitalWrite(LSB_pin + i, (value & 1) >> i);
}
int portRead(uint32_t LSB_pin) {
	int tmp = 0;
	for (int i = 7; i >= 0; i--) tmp = (tmp + digitalRead(LSB_pin + i)) << 1;
	return (uint8_t)tmp;
}


//Private
void	LCD20x4::entry(uint8_t ID, uint8_t SH) { write(0, CMD_Entry_Mode | (ID << 1) | SH); }
void	LCD20x4::disp_ctrl(uint8_t D, uint8_t C, uint8_t B) { write(0, CMD_Disp_Ctrl | (D << 2) | (C << 1) | B); }
void	LCD20x4::shift(uint8_t SC, uint8_t RL) { write(0, CMD_Cursor_SH | (SC << 3) | (RL << 2)); }
void	LCD20x4::func_set(uint8_t DL, uint8_t N, uint8_t F) { write(0, CMD_Func_Set | (DL << 4) | (N << 3) | (F << 2)); }
void	LCD20x4::set_CGRAM(uint8_t Addr) { write(0, CMD_Set_CGRAM | Addr); }
void	LCD20x4::set_DDRAM(uint8_t Addr) { write(0, CMD_Set_DDRAM | Addr); }

uint8_t LCD20x4::read(uint8_t RS)
{
	if(BitMode==BitMode_4)return read_4bit(RS);
	else return read_8bit(RS);
}

void LCD20x4::write(uint8_t RS, uint8_t data)
{
	if (BitMode == BitMode_4)return write_4bit(RS,data);
	else return write_8bit(RS,data);
}

uint8_t LCD20x4::read_8bit(uint8_t RS)
{
	uint8_t res = 0;
	pinOut(RS_pin, RS);
	pinOut(RW_pin, 1);
	pinOut(EN_pin, HIGH);
	res = portIn(D0_pin);
	pinOut(EN_pin, LOW);
	return res;
}

void LCD20x4::write_8bit(uint8_t RS, uint8_t data)
{
	pinOut(RS_pin, RS);
	pinOut(RW_pin, 0);
	portOut(D0_pin,data);
	pinOut(EN_pin, HIGH);
	pinOut(EN_pin, LOW);
	while (chk_busy());
}

uint8_t LCD20x4::read_4bit(uint8_t RS)
{
	uint8_t res = 0;
	pinOut(RS_pin, RS);
	pinOut(RW_pin, 1);
	pinOut(EN_pin, HIGH);

	res = portIn(D4_pin)<<4;
	
	pinOut(EN_pin, LOW);
	pinOut(EN_pin, HIGH);

	res = res | portIn(D4_pin);

	pinOut(EN_pin, LOW);

	return res;
}

void LCD20x4::write_4bit(uint8_t RS, uint8_t data)
{
	data &= 0xFF;
	
	pinOut(RS_pin, RS);
	pinOut(RW_pin, 0);
	
	portOut(D4_pin,data>>4);

	pinOut(EN_pin, HIGH);
	pinOut(EN_pin, LOW);
	
	portOut(D4_pin,data & 0xF);	

	pinOut(EN_pin, HIGH);
	pinOut(EN_pin, LOW);

	while (chk_busy());
}

uint8_t LCD20x4::chk_busy(void) {
	if (read(BitMode) & 0x80)return 1;
	else return 0;
}

void	LCD20x4::write_data(uint8_t Data) 
{ 
	write(1, Data); 	

	X++; 
	if (X >= 20)
	{
		Y++;
		if (Y >= 4)
		{
			Y = 3;
			X = 19;
		}
		else X = 0;
	}
}

uint8_t LCD20x4::read_data(void) 
{ 
	X++;
	if (X >= 20)
	{
		Y++;
		if (Y >= 4)
		{
			Y = 3;
			X = 19;
		}
		else X = 0;
	}

	return read(1); 
}

void	LCD20x4::put(const char c)
{
	uint8_t tx, ty;
	switch (c)
	{
		case '\0':	break;
		case '\r':	X = 0; 
					set_DDRAM(char_addr[Y][X]);
					break;
		case '\n':	if(Y<4)Y++; 
					set_DDRAM(char_addr[Y][X]);
					break;
		case '\b':	if (X > 0)X--;
					else
					{
						if (Y > 0)
						{
							Y--; 
							X = 20;
						}
					}
					tx = X;
					ty = Y;
					set_DDRAM(char_addr[Y][X]);
					write_data(' ');
					X = tx;
					Y = ty;
					break;
		default:	write_data(c);					
					break;
	}
	
}

void LCD20x4::init_4bit(bool Cursor_ON, bool Cursor_Blink){
	delay(500);
	backlight(false);

	pinOut(RS_pin, 0);
	pinOut(RW_pin, 0);

	pinOut(EN_pin, HIGH);
	portOut(D4_pin, 0x30>>4);
	pinOut(EN_pin, LOW);
	delayMicroseconds(5);

	pinOut(EN_pin, HIGH);
	portOut(D4_pin, 0x30>>4);	
	pinOut(EN_pin, LOW);
	delayMicroseconds(1);

	pinOut(EN_pin, HIGH);
	portOut(D4_pin, 0x30>>4);	
	pinOut(EN_pin, LOW);

	pinOut(EN_pin, HIGH);
	portOut(D4_pin, 0x20>>4);	
	pinOut(EN_pin, LOW);

	while (chk_busy());

	func_set(0, 1, 0);						// Function set (Interface is 4 bit long.)

	disp_ctrl(0, 0, 0);						//Display off, Curser off, Blink off
	clr_disp();								//Clear Display and Wait for Busy
	disp_ctrl(1, Cursor_ON, Cursor_Blink);	//Display on, Curser on, Blink on
	entry(1, 0);								//Assign Cursor moving inclease direction, Cursor move

	backlight(true);
}

void LCD20x4::init_8bit(bool Cursor_ON, bool Cursor_Blink){
	delay(500);
	backlight(false);

	pinOut(RS_pin, 0);
	pinOut(RW_pin, 0);
	pinOut(EN_pin, HIGH);
	portOut(D0_pin,0x30);	
	pinOut(EN_pin, LOW);
	delayMicroseconds(5);

	pinOut(EN_pin, HIGH);	
	portOut(D0_pin, 0x30);	
	pinOut(EN_pin, LOW);
	delayMicroseconds(1);

	pinOut(EN_pin, HIGH);
	portOut(D0_pin, 0x30);	
	pinOut(EN_pin, LOW);

	pinOut(EN_pin, HIGH);
	portOut(D0_pin, 0x30);	
	pinOut(EN_pin, LOW);

	while (chk_busy());

	func_set(1, 1, 0);						// Function set (Interface is 8 bit long.)
	disp_ctrl(0, 0, 0);						//Display off, Curser off, Blink off
	clr_disp();								//Clear Display and Wait for Busy
	disp_ctrl(1, Cursor_ON, Cursor_Blink);	//Display on, Curser on, Blink on
	entry(1, 0);								//Assign Cursor moving inclease direction, Cursor move

	backlight(true);
}

//Public
LCD20x4::LCD20x4(uint8_t bitmod){
	BitMode = bitmod;
	X = 0;
	Y = 0;
	RS_pin = 0xFF;
	RW_pin = 0xFF;
	EN_pin = 0xFF;
	BL_pin = 0xFF;
	D0_pin = 0xFF;
	D4_pin = 0xFF;
	
	pinIn   = digitalRead;
	pinOut  = digitalWrite;
	portIn  = portRead;
	portOut = portWrite;
}
void LCD20x4::clr_disp(void) 
{ 
	write(0, CMD_Clear_disp); 
	X = 0;
	Y = 0;
}  

void LCD20x4::home(void)     
{ 
	write(0, CMD_Return_Home);
	X = 0;
	Y = 0;
}

void LCD20x4::begin(bool Cursor_ON, bool Cursor_Blink) {
	if (RS_pin == 0xFF)while (1);
	if (RW_pin == 0xFF)while (1);
	if (EN_pin == 0xFF)while (1);
	if (BL_pin == 0xFF)while (1);
	if (D4_pin == 0xFF)while (1);
	if (D0_pin == 0xFF)BitMode = BitMode_4;

	if (BitMode == BitMode_4)init_4bit(Cursor_ON, Cursor_Blink);
	else init_8bit(Cursor_ON, Cursor_Blink);
}

void LCD20x4::printf(const char *format, ...)
{
	char buf[23];
	uint8_t i;
	va_list args;	
	va_start(args, format);
	vsprintf(buf, format, args);
	perror(buf);
	va_end(args);
	i = 0;
	do {
		put(buf[i++]);
	} while (buf[i] != '\0');
}

void LCD20x4::set_cursor(uint8_t show, uint8_t blink, uint8_t move_direction, uint8_t shift)
{
	disp_ctrl(1, show, blink);
	while (chk_busy() & 0x80);
	entry(move_direction, shift);
	while (chk_busy() & 0x80);
}
void LCD20x4::move_cursor(uint8_t pos) 
{
	Y = pos / 20;
	X = pos % 20;
	set_DDRAM(char_addr[Y][X]);
}
void LCD20x4::move_cursor(uint8_t col, uint8_t row)
{
	X = col;
	Y = row;
	set_DDRAM(char_addr[Y][X]);
}
void LCD20x4::backlight(bool ON)
{
	if (ON)pinOut(BL_pin, HIGH);
	else pinOut(BL_pin, LOW);
}
