#ifndef _LCD20X4_h
#define _LCD20X4_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "arduino.h"
#else
#include "WProgram.h"
#endif
#define BitMode_4 4
#define BitMode_8 8

typedef int  (*pInPtr)(uint32_t);
typedef void (*pOutPtr)(uint32_t,uint32_t);



class LCD20x4
{
	private:
		uint8_t X, Y,BitMode;
		uint8_t read(uint8_t RS);
		void	write(uint8_t RS, uint8_t data);
		uint8_t read_8bit(uint8_t RS);
		void	write_8bit(uint8_t RS,uint8_t data);
		uint8_t read_4bit(uint8_t RS);
		void	write_4bit(uint8_t RS, uint8_t data);
		void	entry(uint8_t ID, uint8_t SH);
		void	disp_ctrl(uint8_t D, uint8_t C, uint8_t B);
		void	shift(uint8_t SC, uint8_t RL);
		void	func_set(uint8_t DL, uint8_t N, uint8_t F);
		void	set_CGRAM(uint8_t Addr);
		void	set_DDRAM(uint8_t Addr);
		uint8_t	chk_busy(void);
		void	write_data(uint8_t Data);
		uint8_t read_data(void);
		void	init_4bit(bool Cursor_ON = true, bool Cursor_Blink = true);
		void	init_8bit(bool Cursor_ON = true, bool Cursor_Blink = true);
	protected:
		
	public:
		uint32_t RS_pin, RW_pin, EN_pin, BL_pin, D0_pin, D4_pin;
		pInPtr  pinIn, portIn;
		pOutPtr	pinOut, portOut;
		LCD20x4(uint8_t bitmod);
		void clr_disp(void);	
		void home(void);
		void begin(bool Cursor_ON = true, bool Cursor_Blink = true);
		void put(const char c);
		void printf(const char *format, ...);
		void set_cursor(uint8_t show, uint8_t blink, uint8_t move_direction, uint8_t shift);
		void move_cursor(uint8_t pos);
		void move_cursor(uint8_t col, uint8_t row);
		void backlight(bool ON);
};

#endif